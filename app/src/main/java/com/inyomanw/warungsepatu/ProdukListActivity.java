package com.inyomanw.warungsepatu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.inyomanw.warungsepatu.adapter.BarangAdapter;
import com.inyomanw.warungsepatu.base.api.ApiClient;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.base.constant.Constant;
import com.inyomanw.warungsepatu.model.Barang;
import com.inyomanw.warungsepatu.model.Produk;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProdukListActivity extends AppCompatActivity implements BarangAdapter.ItemListener {

    ApiInterface mApiInterface;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager rvLiLayoutManager;
    BarangAdapter barangAdapter;

    public final static String TAG = "ProdukListActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk_list);

        //set recycleView
        recyclerView = findViewById(R.id.rv);
        rvLiLayoutManager = new GridLayoutManager(this,2);
        //rvLiLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        recyclerView.setLayoutManager(rvLiLayoutManager);

        refresh();
    }

    public void refresh() {
        Call<Produk> produkcall = mApiInterface.getProduk();
        produkcall.enqueue(new Callback<Produk>() {
            @Override
            public void onResponse(Call<Produk> call, Response<Produk> response) {
                List<Barang> produks = response.body().getBarangs();
                barangAdapter = new BarangAdapter(ProdukListActivity.this, produks,ProdukListActivity.this);
                recyclerView.setAdapter(barangAdapter);
            }

            @Override
            public void onFailure(Call<Produk> call, Throwable t) {
                Toast.makeText(ProdukListActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(Barang barang) {
        Intent intent = new Intent(this,DetailProdukActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constant.Key.produk,barang);
        intent.putExtra(Constant.Key.produk,barang);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
