package com.inyomanw.warungsepatu.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.inyomanw.warungsepatu.DetailProdukActivity;
import com.inyomanw.warungsepatu.ProdukListActivity;
import com.inyomanw.warungsepatu.R;
import com.inyomanw.warungsepatu.adapter.BarangAdapter;
import com.inyomanw.warungsepatu.base.api.ApiClient;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.base.constant.Constant;
import com.inyomanw.warungsepatu.model.Barang;
import com.inyomanw.warungsepatu.model.Produk;
import com.inyomanw.warungsepatu.model.test;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements BarangAdapter.ItemListener {

    ApiInterface mApiInterface;
    RecyclerView recyclerView,rvOther;
    ArrayList<Barang> baranglist;
    BarangAdapter barangAdapter;
    RecyclerView.LayoutManager rvLiLayoutManager, rvLiLayoutManagerOther;
    TextView tvMore;

    public final static String TAG = "HomeFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //set recycleView
        recyclerView = view.findViewById(R.id.rv_produk);
        TextView tv_rekomendasi = view.findViewById(R.id.tv_rekomendasi);
        //rvLiLayoutManager = new GridLayoutManager(getContext(),2);
        rvLiLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        recyclerView.setLayoutManager(rvLiLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        rvOther = view.findViewById(R.id.rv_other);
        rvLiLayoutManagerOther = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        rvOther.setLayoutManager(rvLiLayoutManagerOther);
        refresh();

        //asdfasf
        tvMore = view.findViewById(R.id.tv_more);
        tvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProdukListActivity.class);
                startActivity(intent);
            }
        });

        tv_rekomendasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                testpush();
                testget();
            }
        });

    }

    public void refresh() {
        final Call<Produk> produkcall = mApiInterface.getProduk();
        produkcall.enqueue(new Callback<Produk>() {
            @Override
            public void onResponse(Call<Produk> call, Response<Produk> response) {
                List<Barang> produks = response.body().getBarangs();
                List<Barang> produks2=new ArrayList<>();
                for(int i=0;i<produks.size();i++){
                    if(i<5){
                        produks2.add(produks.get(i));
                    }
                }
                barangAdapter = new BarangAdapter(getActivity(), produks2,HomeFragment.this);
                recyclerView.setAdapter(barangAdapter);
                rvOther.setAdapter(barangAdapter);
            }

            @Override
            public void onFailure(Call<Produk> call, Throwable t) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void testpush(){
        final Call<String> stringCall = mApiInterface.favorit("favorit1607", "brg226");
        stringCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(getActivity(), response.body(), Toast.LENGTH_SHORT ).show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void testget(){
        final Call<test> produkCall = mApiInterface.getProdukById("brg22");
        produkCall.enqueue(new Callback<test>() {
            @Override
            public void onResponse(Call<test> call, Response<test> response) {
                Toast.makeText(getActivity(), response.body().getBarang().getIdbarang(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<test> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(Barang barang) {
        Intent intent = new Intent(getActivity(),DetailProdukActivity.class);
        Bundle bundle = new Bundle();

        bundle.putParcelable(Constant.Key.produk,barang);
        intent.putExtra(Constant.Key.produk,barang);
        startActivity(intent);
    }
}
