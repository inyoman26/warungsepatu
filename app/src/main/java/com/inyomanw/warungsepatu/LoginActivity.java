package com.inyomanw.warungsepatu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.inyomanw.warungsepatu.base.SharedPreference;
import com.inyomanw.warungsepatu.base.api.ApiClient;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.model.loginmodel;

import java.util.Arrays;

import customfonts.MyEditText;
import customfonts.MyTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    LinearLayout loginFB,loginGoogle;
    private Boolean exit = false;
    private static final String TAG = "FACELOG";
    public MyTextView signup, signin1;
    public MyEditText email, password;
    public ApiInterface mApiInterface;
    public SharedPreference sharedPreference;
    public String bundleexit = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String test = getIntent().getStringExtra("Exit");
        if(test != null){
            bundleexit = test;
        }

        sharedPreference = new SharedPreference();
        String temp = sharedPreference.getValue(getApplicationContext());
        if(temp != null){
            Intent it = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(it);
        }

        setContentView(R.layout.activity_login);
        loginFB = findViewById(R.id.loginfb);
        signup = findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(it);
            }
        });
        signin1 = findViewById(R.id.signin1);
        signin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLogin();
            }
        });
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

    }



    public void onLogin(){
        String emailtemp = email.getText().toString();
        String pass = password.getText().toString();
        try{
            final Call<loginmodel> login = mApiInterface.loginAccount(emailtemp, pass);
            login.enqueue(new Callback<loginmodel>() {
                @Override
                public void onResponse(Call<loginmodel> call, Response<loginmodel> response) {
                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        sharedPreference.save(getApplicationContext(), response.body().getIdpengguna());
                        Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(it);

                    } else{
                        Toast.makeText(LoginActivity.this, response.body().getMessage() , Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<loginmodel> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, "Gagal login" , Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e){
            Toast.makeText(LoginActivity.this, e.getMessage() , Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!bundleexit.isEmpty()){
            finishAffinity();
            System.exit(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
