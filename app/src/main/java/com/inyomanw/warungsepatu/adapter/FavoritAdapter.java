package com.inyomanw.warungsepatu.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.inyomanw.warungsepatu.ProdukListActivity;
import com.inyomanw.warungsepatu.R;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.model.Barang;
import com.inyomanw.warungsepatu.model.Favorit;
import com.inyomanw.warungsepatu.model.Produk;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FavoritAdapter extends RecyclerView.Adapter<FavoritAdapter.ViewHolder> {
    Context context;
    ItemListener itemListener;
    private List<Favorit> mList;
    ApiInterface mApiInterface;

    public FavoritAdapter(Context context, List<Favorit> mList,ItemListener itemListener) {
        this.context = context;
        this.itemListener = itemListener;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.rv_favorit,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder instanceof FavoritAdapter.ViewHolder)
        {
            final String[] gambar = new String[1];
            final Favorit item = mList.get(position);
            holder.tvNama.setText(item.getNamabarang());
            holder.tvMerk.setText(item.getMerk());
            String temp = NumberFormat.getNumberInstance(Locale.UK).format(Integer.parseInt(item.getHarga()));
            holder.tvHarga.setText("Rp"+temp);

            Call<Produk> produkcall = mApiInterface.getProduk();
            produkcall.enqueue(new Callback<Produk>() {
                @Override
                public void onResponse(Call<Produk> call, Response<Produk> response) {
                    List<Barang> produks = response.body().getBarangs();
                    gambar[0] = produks.get(0).getGambars().get(0).getGambar();
                }

                @Override
                public void onFailure(Call<Produk> call, Throwable t) {

                }
            });

            Glide.with(context)
                    .load("http://inyomanw.com/WarungSepatu/"+ gambar[0])
                    .asBitmap()
                    .centerCrop()
                    .into(holder.img);

            holder.btnPesan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //pindah ke activity detail parsing data by idbarang
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvNama, tvMerk, tvHarga;
        Button btnPesan;
        ConstraintLayout constraintLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_favorit);
            tvNama=itemView.findViewById(R.id.tv_namaproduk);
            tvMerk=itemView.findViewById(R.id.tv_merk);
            tvHarga = itemView.findViewById(R.id.tv_harga);
            btnPesan = itemView.findViewById(R.id.btn_pesan);
            constraintLayout = itemView.findViewById(R.id.rv_favorit);
        }
    }
    public interface ItemListener
    {
        //void onItemClick(Favorit favorit)
    }
}
