package com.inyomanw.warungsepatu.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.inyomanw.warungsepatu.R;
import com.inyomanw.warungsepatu.model.Barang;
import com.inyomanw.warungsepatu.model.Produk;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder> {
    private Context mContext;
    private List<Barang> mList = new ArrayList<>();
    private ItemListener itemListener;

    public BarangAdapter(Context context, List<Barang> list, ItemListener itemListener)
    {
        mContext=context;
        mList=list;
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.rv_produk_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(holder instanceof BarangAdapter.ViewHolder)
        {
            final Barang item = mList.get(position);
            TextView nama_barang = holder.item_name;
            TextView merk = holder.item_merk;
            TextView harga = holder.item_price;
            ImageView gambar_barang = holder.item_image;

            nama_barang.setText(item.getNamabarang());
            merk.setText(item.getMerk());
            String temp = NumberFormat.getNumberInstance(Locale.UK).format(Integer.parseInt(item.getHarga()));
            harga.setText("Rp"+temp);
            Glide.with(mContext)
                    .load("http://inyomanw.com/WarungSepatu/"+item.getGambars().get(0).getGambar())
                    .asBitmap()
                    .centerCrop()
                    .into(gambar_barang);

            holder.cardview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemListener.onItemClick(item);
                }
            });


        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView item_image;
        TextView item_name, item_merk,item_price;
        CardView cardview;

        public  ViewHolder(View itemView)
        {
            super((itemView));
            item_image= itemView.findViewById(R.id.imgBarang);
            item_name= itemView.findViewById(R.id.tvProdukName);
            item_price=itemView.findViewById(R.id.tvPrice);
            item_merk=itemView.findViewById(R.id.tvMerk);
            cardview = itemView.findViewById(R.id.cardview);
        }
    }

    public interface ItemListener
    {
        void onItemClick(Barang barang);
    }
}
