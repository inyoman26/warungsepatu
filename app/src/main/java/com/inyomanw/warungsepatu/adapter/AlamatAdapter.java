package com.inyomanw.warungsepatu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.inyomanw.warungsepatu.R;
import com.inyomanw.warungsepatu.model.Alamat;

import java.util.List;

public class AlamatAdapter extends RecyclerView.Adapter<AlamatAdapter.ViewHolder>{
    private Context context;
    private List<Alamat> mList;
    private ItemListener itemListener;

    public AlamatAdapter(Context context, List<Alamat> mList, ItemListener itemListener) {
        this.context = context;
        this.mList = mList;
        this.itemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.rv_alamat,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder instanceof AlamatAdapter.ViewHolder)
        {
            final Alamat item = mList.get(position);
            holder.tvDeskripsi.setText(item.getKeterangan());
            holder.tvPenerima.setText(item.getNamapenerima());
            holder.tvAlamat.setText(item.getAlamat());
            holder.tvKecamatan.setText(item.getKecamatan());
            holder.tvKabupaten.setText(item.getKabupaten());
            holder.tvKodepos.setText(item.getKodepos());
            holder.tvProvinsi.setText(item.getProvinsi());
            holder.tvNohp.setText(item.getNohp());

            holder.imvEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemListener.onItemClick(item);
                    //intent
                }
            });

            holder.imvHapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDeskripsi, tvPenerima,tvAlamat,tvKecamatan,
                tvKabupaten,tvKodepos,tvProvinsi,tvNohp;
        ImageView imvHapus, imvEdit;
        public ViewHolder(View itemView) {
            super(itemView);
            tvDeskripsi = itemView.findViewById(R.id.tv_deskripsi);
            tvPenerima = itemView.findViewById(R.id.tv_penerima);
            tvAlamat = itemView.findViewById(R.id.tv_alamat);
            tvKecamatan = itemView.findViewById(R.id.tv_kecamatan);
            tvKabupaten = itemView.findViewById(R.id.tv_kabupaten);
            tvKodepos = itemView.findViewById(R.id.tv_kodepos);
            tvProvinsi = itemView.findViewById(R.id.tv_provinsi);
            tvNohp = itemView.findViewById(R.id.tv_nohp);
            imvHapus = itemView.findViewById(R.id.imv_hapus);
            imvEdit = itemView.findViewById(R.id.imv_edit);
        }
    }
    public interface ItemListener
    {
        void onItemClick(Alamat alamat);
    }
}
