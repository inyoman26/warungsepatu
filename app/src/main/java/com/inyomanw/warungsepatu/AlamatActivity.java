package com.inyomanw.warungsepatu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.inyomanw.warungsepatu.adapter.AlamatAdapter;
import com.inyomanw.warungsepatu.base.SharedPreference;
import com.inyomanw.warungsepatu.base.api.ApiClient;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.model.Alamat;
import com.inyomanw.warungsepatu.model.AlamatbyId;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlamatActivity extends AppCompatActivity implements AlamatAdapter.ItemListener {

    private ApiInterface mApiInterface;
    private SharedPreference sharedPreference;
    private ArrayList<Alamat> alamatlist;
    private String idpengguna;
    private AlamatAdapter alamatAdapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager rvLayoutManager;
    private ImageView btnTambah,btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alamat);

        sharedPreference = new SharedPreference();
        idpengguna = sharedPreference.getValue(getApplicationContext());
        recyclerView = findViewById(R.id.rv_alamat);
        rvLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(rvLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        getAlamat();
        btnTambah = findViewById(R.id.btn_tambah);
        btnBack = findViewById(R.id.btn_back);
        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new  Intent(AlamatActivity.this,AddAlamatActivity.class);
                startActivity(intent);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void getAlamat()
    {
        final Call<AlamatbyId> alamatbyIdCall = mApiInterface.getAlamatById(idpengguna);
        alamatbyIdCall.enqueue(new Callback<AlamatbyId>() {
            @Override
            public void onResponse(Call<AlamatbyId> call, Response<AlamatbyId> response) {
                List<Alamat> alamats = response.body().getAlamat();
                alamatAdapter = new AlamatAdapter(getApplicationContext(),alamats,AlamatActivity.this);
                recyclerView.setAdapter(alamatAdapter);
            }

            @Override
            public void onFailure(Call<AlamatbyId> call, Throwable t) {

            }
        });
    }

    @Override
    public void onItemClick(Alamat alamat) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
