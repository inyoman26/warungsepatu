package com.inyomanw.warungsepatu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.inyomanw.warungsepatu.adapter.FavoritAdapter;
import com.inyomanw.warungsepatu.base.api.ApiInterface;

public class FavoritActivity extends AppCompatActivity {

    ApiInterface mApiInterface;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager rvLiLayoutManager;
    FavoritAdapter favoritAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorit);
    }
}
