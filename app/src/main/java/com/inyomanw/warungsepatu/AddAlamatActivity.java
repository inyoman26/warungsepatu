package com.inyomanw.warungsepatu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.inyomanw.warungsepatu.base.SharedPreference;
import com.inyomanw.warungsepatu.base.api.ApiClient;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.model.loginmodel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAlamatActivity extends AppCompatActivity {
    private ApiInterface mApiInterface;
    private EditText edtNamaPenerima,edtAlamat,edtKecamatan,edtKabupaten,edtProvinsi;
    private EditText edtKodePos,edtKeterangan,edtNoHP;
    private Button btnSimpan;
    private SharedPreference sharedPreference;
    private String idpengguna,namapenerima,alamat,kecamatan,kabupaten,provinsi,kodepos,keterangan,nohp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_alamat);
        initLayout();
        sharedPreference = new SharedPreference();
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        idpengguna = sharedPreference.getValue(getApplicationContext());
        Toast.makeText(AddAlamatActivity.this,idpengguna,Toast.LENGTH_SHORT).show();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tambahAlamat();
            }
        });
    }
    private void initLayout()
    {
        edtNamaPenerima = findViewById(R.id.edt_namapenerima);
        edtAlamat = findViewById(R.id.edt_alamat);
        edtKecamatan = findViewById(R.id.edt_kecamatan);
        edtKabupaten = findViewById(R.id.edt_kabupaten);
        edtProvinsi = findViewById(R.id.edt_provinsi);
        edtKodePos = findViewById(R.id.edt_kodepos);
        edtKeterangan = findViewById(R.id.edt_deskripsi);
        edtNoHP = findViewById(R.id.edt_nohp);
        btnSimpan = findViewById(R.id.btn_simpan);

    }

    private void tambahAlamat(){
        idpengguna = sharedPreference.getValue(getApplicationContext());
        namapenerima = edtNamaPenerima.getText().toString();
        alamat = edtAlamat.getText().toString();
        kecamatan = edtKecamatan.getText().toString();
        kabupaten = edtKabupaten.getText().toString();
        provinsi = edtProvinsi.getText().toString();
        kodepos = edtKodePos.getText().toString();
        keterangan = edtKeterangan.getText().toString();
        nohp = edtNoHP.getText().toString();

        try{
            final Call<loginmodel> addAlamat = mApiInterface.addAlamat(idpengguna,namapenerima,alamat,kecamatan,kabupaten,provinsi,kodepos,keterangan,nohp);
            addAlamat.enqueue(new Callback<loginmodel>() {
                @Override
                public void onResponse(Call<loginmodel> call, Response<loginmodel> response) {
                    if(response.body().getStatus().equalsIgnoreCase("Success")){
                        Toast.makeText(AddAlamatActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<loginmodel> call, Throwable t) {
                    Toast.makeText(AddAlamatActivity.this, t.getMessage(),Toast.LENGTH_SHORT).show();
                    //gak man. tu throw dari retrofitnya dia emg manggil atau gak oke"

                }
            });
        }
        catch (Exception e)
        {
            Toast.makeText(AddAlamatActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }
}
