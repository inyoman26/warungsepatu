package com.inyomanw.warungsepatu;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.inyomanw.warungsepatu.base.api.ApiClient;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.fragments.HomeFragment;
import com.inyomanw.warungsepatu.model.loginmodel;
import com.inyomanw.warungsepatu.model.test;

import customfonts.MyEditText;
import customfonts.MyTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    public MyTextView signin, Register;
    public MyEditText username, password, email, phone;
    public CheckBox checkbocremember;
    public ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        signin = findViewById(R.id.signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(it);
            }
        });

        Register = findViewById(R.id.signin1);
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRegister();
            }
        });

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);

        checkbocremember = findViewById(R.id.checkbocremember);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

    }

    public void onRegister(){
        if(checkbocremember.isChecked()){
            String uname = username.getText().toString();
            String pass = password.getText().toString();
            String emailtemp = email.getText().toString();
            String phonenumber = phone.getText().toString();
            try{
                final Call<loginmodel> register = mApiInterface.registerAccount(uname, pass, emailtemp, phonenumber);
                register.enqueue(new Callback<loginmodel>() {
                    @Override
                    public void onResponse(Call<loginmodel> call, Response<loginmodel> response) {
                        if(response.body().getStatus().equalsIgnoreCase("success")){
                            Toast.makeText(RegisterActivity.this, "Registrasi berhasil, Silahkan login", Toast.LENGTH_SHORT).show();
                            Intent it = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(it);

                        } else{
                            Toast.makeText(RegisterActivity.this, response.body().getMessage() , Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<loginmodel> call, Throwable t) {
                        Toast.makeText(RegisterActivity.this, "Gagal registrasi" , Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e){
                Toast.makeText(RegisterActivity.this, e.getMessage() , Toast.LENGTH_SHORT).show();
            }
        } else{
            Toast.makeText(this, "Harap centang Term & Condition", Toast.LENGTH_SHORT).show();
        }
    }
}
