package com.inyomanw.warungsepatu.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Produk {
    @SerializedName("barangs")
    private List<Barang> barangs;

    public List<Barang> getBarangs() {
        return barangs;
    }

    public void setBarangs(List<Barang> barangs) {
        this.barangs = barangs;
    }
}
