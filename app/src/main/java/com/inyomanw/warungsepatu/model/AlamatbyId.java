package com.inyomanw.warungsepatu.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlamatbyId {
    @SerializedName("alamat")
    private List<Alamat> alamat;

    public List<Alamat> getAlamat() {
        return alamat;
    }

    public void setAlamat(List<Alamat> alamat) {
        this.alamat = alamat;
    }

    //ini kok list alamat man? alamatnya bisa lebih dari 1?
    //iya bob, emg bsa lebih ok
}
