package com.inyomanw.warungsepatu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Barang implements Parcelable {

    @SerializedName("idbarang")
    private String idbarang;
    @SerializedName("namabarang")
    private String namabarang;
    @SerializedName("merk")
    private String merk;
    @SerializedName("harga")
    private String harga;
    @SerializedName("deskripsi")
    private String deskripsi;
    @SerializedName("terjual")
    private String terjual;

    @SerializedName("gambars")
    private List<Gambar> gambars;
    @SerializedName("sizebarangs")
    private List<SizeBarang> sizebarangs;


    protected Barang(Parcel in) {
        idbarang = in.readString();
        namabarang = in.readString();
        merk = in.readString();
        harga = in.readString();
        deskripsi = in.readString();
        terjual = in.readString();
        sizebarangs = in.createTypedArrayList(SizeBarang.CREATOR);
        gambars = in.createTypedArrayList(Gambar.CREATOR);
    }

    public static final Creator<Barang> CREATOR = new Creator<Barang>() {
        @Override
        public Barang createFromParcel(Parcel in) {
            return new Barang(in);
        }

        @Override
        public Barang[] newArray(int size) {
            return new Barang[size];
        }
    };

    public String getIdbarang() {
        return idbarang;
    }

    public void setIdbarang(String idbarang) {
        this.idbarang = idbarang;
    }

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTerjual() {
        return terjual;
    }

    public void setTerjual(String terjual) {
        this.terjual = terjual;
    }

    public List<Gambar> getGambars() {
        return gambars;
    }

    public void setGambars(List<Gambar> gambars) {
        this.gambars = gambars;
    }

    public List<SizeBarang> getSizebarangs() {
        return sizebarangs;
    }

    public void setSizebarangs(List<SizeBarang> sizebarangs) {
        this.sizebarangs = sizebarangs;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idbarang);
        parcel.writeString(namabarang);
        parcel.writeString(merk);
        parcel.writeString(harga);
        parcel.writeString(deskripsi);
        parcel.writeString(terjual);
        parcel.writeTypedList(sizebarangs);
        parcel.writeTypedList(gambars);
    }
}
