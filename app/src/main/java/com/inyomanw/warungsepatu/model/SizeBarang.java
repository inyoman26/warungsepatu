package com.inyomanw.warungsepatu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SizeBarang implements Parcelable {
    @SerializedName("idsizebarang")
    private String idsizebarang;
    @SerializedName("idbarang")
    private String idbarang;
    @SerializedName("size")
    private int size;
    @SerializedName("stock")
    private int stock;

    protected SizeBarang(Parcel in) {
        idsizebarang = in.readString();
        idbarang = in.readString();
        size = in.readInt();
        stock = in.readInt();
    }

    public static final Creator<SizeBarang> CREATOR = new Creator<SizeBarang>() {
        @Override
        public SizeBarang createFromParcel(Parcel in) {
            return new SizeBarang(in);
        }

        @Override
        public SizeBarang[] newArray(int size) {
            return new SizeBarang[size];
        }
    };

    public String getIdsizebarang() {
        return idsizebarang;
    }

    public void setIdsizebarang(String idsizebarang) {
        this.idsizebarang = idsizebarang;
    }

    public String getIdbarang() {
        return idbarang;
    }

    public void setIdbarang(String idbarang) {
        this.idbarang = idbarang;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idsizebarang);
        parcel.writeString(idbarang);
        parcel.writeInt(size);
        parcel.writeInt(stock);
    }
}
