package com.inyomanw.warungsepatu.model;

import com.google.gson.annotations.SerializedName;

public class test {
    @SerializedName("barangs")
    private Barang barang;

    public Barang getBarang() {
        return barang;
    }

    public void setBarang(Barang barang) {
        this.barang = barang;
    }
}
