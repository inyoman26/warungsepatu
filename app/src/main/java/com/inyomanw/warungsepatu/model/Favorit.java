package com.inyomanw.warungsepatu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Favorit implements Parcelable {
    @SerializedName("idbarang")
    private String idbarang;
    @SerializedName("namabarang")
    private String namabarang;
    @SerializedName("merk")
    private String merk;
    @SerializedName("harga")
    private String harga;
    @SerializedName("deskripsi")
    private String deskripsi;

    protected Favorit(Parcel in) {
        idbarang = in.readString();
        namabarang = in.readString();
        merk = in.readString();
        harga = in.readString();
        deskripsi = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idbarang);
        dest.writeString(namabarang);
        dest.writeString(merk);
        dest.writeString(harga);
        dest.writeString(deskripsi);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Favorit> CREATOR = new Creator<Favorit>() {
        @Override
        public Favorit createFromParcel(Parcel in) {
            return new Favorit(in);
        }

        @Override
        public Favorit[] newArray(int size) {
            return new Favorit[size];
        }
    };

    public static Creator<Favorit> getCREATOR() {
        return CREATOR;
    }

    public String getIdbarang() {
        return idbarang;
    }

    public void setIdbarang(String idbarang) {
        this.idbarang = idbarang;
    }

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

}
