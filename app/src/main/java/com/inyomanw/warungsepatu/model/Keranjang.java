package com.inyomanw.warungsepatu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Keranjang implements Parcelable {
    @SerializedName("idbarang")
    private String idbarang;
    @SerializedName("namabarang")
    private String namabarang;
    @SerializedName("merk")
    private String merk;
    @SerializedName("harga")
    private int harga;
    @SerializedName("size")
    private int size;
    @SerializedName("idsizebarang")
    private String idsizebarang;
    @SerializedName("iddetailkeranjang")
    private  String iddetailkeranjang;
    @SerializedName("jumlah")
    private int jumlah;

    protected Keranjang(Parcel in) {
        idbarang = in.readString();
        namabarang = in.readString();
        merk = in.readString();
        harga = in.readInt();
        size = in.readInt();
        idsizebarang = in.readString();
        iddetailkeranjang = in.readString();
        jumlah = in.readInt();
    }

    public static Creator<Keranjang> getCREATOR() {
        return CREATOR;
    }

    public static final Creator<Keranjang> CREATOR = new Creator<Keranjang>() {
        @Override
        public Keranjang createFromParcel(Parcel in) {
            return new Keranjang(in);
        }

        @Override
        public Keranjang[] newArray(int size) {
            return new Keranjang[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idbarang);
        parcel.writeString(namabarang);
        parcel.writeString(merk);
        parcel.writeInt(harga);
        parcel.writeInt(size);
        parcel.writeString(idsizebarang);
        parcel.writeString(iddetailkeranjang);
        parcel.writeInt(jumlah);
    }

    public String getIdbarang() {
        return idbarang;
    }

    public void setIdbarang(String idbarang) {
        this.idbarang = idbarang;
    }

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getIdsizebarang() {
        return idsizebarang;
    }

    public void setIdsizebarang(String idsizebarang) {
        this.idsizebarang = idsizebarang;
    }

    public String getIddetailkeranjang() {
        return iddetailkeranjang;
    }

    public void setIddetailkeranjang(String iddetailkeranjang) {
        this.iddetailkeranjang = iddetailkeranjang;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
