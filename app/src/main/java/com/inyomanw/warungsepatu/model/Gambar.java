package com.inyomanw.warungsepatu.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Gambar implements Parcelable{
    @SerializedName("idgambar")
    private String idgambar;
    @SerializedName("idbarang")
    private String idbarang;
    @SerializedName("gambar")
    private String gambar;

    protected Gambar(Parcel in) {
        idgambar = in.readString();
        idbarang = in.readString();
        gambar = in.readString();
    }

    public static final Creator<Gambar> CREATOR = new Creator<Gambar>() {
        @Override
        public Gambar createFromParcel(Parcel in) {
            return new Gambar(in);
        }

        @Override
        public Gambar[] newArray(int size) {
            return new Gambar[size];
        }
    };

    public String getIdgambar() {
        return idgambar;
    }

    public void setIdgambar(String idgambar) {
        this.idgambar = idgambar;
    }

    public String getIdbarang() {
        return idbarang;
    }

    public void setIdbarang(String idbarang) {
        this.idbarang = idbarang;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(idgambar);
        parcel.writeString(idbarang);
        parcel.writeString(gambar);
    }
}
