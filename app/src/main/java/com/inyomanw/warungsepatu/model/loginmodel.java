package com.inyomanw.warungsepatu.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by CIBRO on 6/21/2018.
 */

public class loginmodel {
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;
    @SerializedName("idpengguna")
    private String idpengguna;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdpengguna() {
        return idpengguna;
    }

    public void setIdpengguna(String idpengguna) {
        this.idpengguna = idpengguna;
    }
}
