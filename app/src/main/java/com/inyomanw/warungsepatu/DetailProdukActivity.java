package com.inyomanw.warungsepatu;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.inyomanw.warungsepatu.adapter.ViewPagerAdapter;
import com.inyomanw.warungsepatu.base.api.ApiInterface;
import com.inyomanw.warungsepatu.base.constant.Constant;
import com.inyomanw.warungsepatu.model.Barang;

import java.text.NumberFormat;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProdukActivity extends AppCompatActivity {

    private static final String EXTRA_BARANG = "BARANG";
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    ImageView imvGambar;
    TextView tvHarga,tvNamaBarang;
    Spinner spinnerUkuran;
    Integer temp;
    Button addKeranjang;
    TabLayout tabLayout;
    ApiInterface mApiInterface;
    String idsizebarang;
    ImageView ImvBack;
    TextView txtDeskripsi;



    private final static String TAG = "DetailProdukActivity";

//    public static Intent getcallingintent(Context context, Barang barang){
//        Intent intent = new Intent(context, DetailProdukActivity.class);
//        Bundle args = new Bundle();
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);

        viewPager = findViewById(R.id.imv_barang);
        tabLayout = findViewById(R.id.tab_dots);
        //imvGambar = findViewById(R.id.imv_barang);
        tvHarga = findViewById(R.id.tv_harga);
        tvNamaBarang = findViewById(R.id.tv_namabarang);
        spinnerUkuran = findViewById(R.id.spinner_ukurann);
        addKeranjang = findViewById(R.id.addKeranjang);
        ImvBack = findViewById(R.id.ImvBack);
        txtDeskripsi = findViewById(R.id.txtDeskripsi);

        final Barang barang = getIntent().getParcelableExtra(Constant.Key.produk);


        String harga = NumberFormat.getNumberInstance(Locale.UK).format(Integer.parseInt(barang.getHarga()));
        tvHarga.setText("Rp" + harga);
        tvNamaBarang.setText(barang.getNamabarang());
        txtDeskripsi.setText(barang.getDeskripsi());
        //viewPagerAdapter = new ViewPagerAdapter(DetailProdukActivity.this,gambars);

        getGambars();
        getUkuranBarang();


        addKeranjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(int i=0;i<barang.getSizebarangs().size();i++){
                    if(barang.getSizebarangs().get(i).getSize() == temp){
                        idsizebarang = barang.getSizebarangs().get(i).getIdsizebarang();
                    }
                }
                addKeranjang("user1", barang.getIdbarang(), idsizebarang, "1");
            }
        });

        ImvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void addKeranjang(String idpengguna, String idsizepengguna, String idbarang, String jumlah){
        try{
            final Call<String> stringCall = mApiInterface.addKeranjang(idpengguna, idsizepengguna, idbarang, jumlah);
            stringCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Toast.makeText(DetailProdukActivity.this, response.body(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Toast.makeText(DetailProdukActivity.this, "Gagal", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e){
            Toast.makeText(DetailProdukActivity.this, e.getMessage() , Toast.LENGTH_SHORT).show();
        }
    }


    private void getUkuranBarang(){
        Barang barang = getIntent().getParcelableExtra(Constant.Key.produk);
        if(barang.getSizebarangs() != null && barang.getSizebarangs().size() > 0){
            Integer[] ukuran = new Integer[barang.getSizebarangs().size()];
            for(int i=0;i<barang.getSizebarangs().size();i++){
                ukuran[i] = barang.getSizebarangs().get(i).getSize();
            }

            final ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, ukuran);

            spinnerUkuran.setAdapter(adapter);
            spinnerUkuran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    temp = adapter.getItem(i);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }
    private void getGambars()
    {
        Barang barang = getIntent().getParcelableExtra(Constant.Key.produk);
        if(barang.getGambars() != null && barang.getGambars().size() >0)
        {
            String[] gambars = new String[barang.getGambars().size()];
            for (int i=0;i<barang.getGambars().size();i++)
            {
                gambars[i]=barang.getGambars().get(i).getGambar();
            }
            viewPagerAdapter = new ViewPagerAdapter(DetailProdukActivity.this,gambars);
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager,true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
