package com.inyomanw.warungsepatu.base.api;

import com.inyomanw.warungsepatu.model.AlamatbyId;
import com.inyomanw.warungsepatu.model.Produk;
import com.inyomanw.warungsepatu.model.loginmodel;
import com.inyomanw.warungsepatu.model.test;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("Barang")
    Call<Produk> getProduk();

    @GET("Barang/{idbarang}")
    Call<test> getProdukById(@Path("idbarang") String idbarang);

    @GET("Alamat/{idalamat}")
    Call<AlamatbyId> getAlamatById(@Path("idalamat")String idalamat);

    @FormUrlEncoded
    @POST("Alamat")
    Call<loginmodel> addAlamat(@Field("idpengguna") String idpengguna,
                           @Field("namapenerima") String namapenerima,
                           @Field("alamat") String alamat,
                           @Field("kecamatan") String kecamatan,
                           @Field("kabupaten") String kabupaten,
                           @Field("provinsi") String provinsi,
                           @Field("kodepos") String kodepos,
                           @Field("keterangan") String keterangan,
                           @Field("nohp") String nohp
                           );

    @FormUrlEncoded
    @POST("DetailFavorit")
    Call<String> favorit(@Field("idfavorit") String idfavorit,
                         @Field("idbarang") String idbarang);

    @FormUrlEncoded
    @POST("Favorit")
    Call<String> addfavorit(@Field("idpengguna") String idpengguna,
                            @Field("idbarang") String idbarang);

    @FormUrlEncoded
    @POST("Keranjang")
    Call<String> addKeranjang(@Field("idpengguna") String idpengguna,
                                    @Field("idsizebarang") String idsizebarang,
                                    @Field("idbarang") String idbarang,
                                    @Field("jumlah") String jumlah);

    @FormUrlEncoded
    @POST("Register")
    Call<loginmodel> registerAccount(@Field("namapengguna") String namapengguna,
                                     @Field("nohp") String nohp,
                                     @Field("email") String email,
                                     @Field("password") String password);

    @FormUrlEncoded
    @POST("Login")
    Call<loginmodel> loginAccount(@Field("email") String email,
                                  @Field("password") String password);
}
